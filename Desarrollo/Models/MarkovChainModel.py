import seaborn as sns; sns.set_theme()
import matplotlib.pyplot as plot
import numpy as np
from markovclick.viz import visualise_markov_chain
from markovclick.models import MarkovClickstream
from markovclick import dummy

# Markov Chain Model for Sequences
class MarkovChainModel:

    def __init__(self, virtual_space_name: str, room: str) -> None:
        self.stream_sequences = []
        self.stream_sequences_list = []
        self.room = room
        self.virtual_space_name = virtual_space_name
        self.max_nodes = 1
        self.markov_model = None

        pass
    
    # Calculate page rank
    def __calculte_markov_model_page_rank(self):
        max_stream_lenght = -1
        for stream in self.stream_sequences_list:
            a_set = set(stream)
            length = len(a_set)
            if length > max_stream_lenght:
                max_stream_lenght = length

       
        if (max_stream_lenght == 1):
            self.max_nodes = 1
        else:
            self.max_nodes = 2
        self.markov_model.calculate_pagerank(max_nodes=self.max_nodes)

    # Constructs Markov model, if exists sequences add news
    def construct_model(self, stream_sequences):
        self.stream_sequences.append(stream_sequences)
        
        self.stream_sequences_list = []
        for item in self.stream_sequences:
            cols = len(item)
            col = 0
            while col < cols:
                col_list = item[col]
                self.stream_sequences_list.append(col_list)
                col = col + 1

        self.markov_model = MarkovClickstream(self.stream_sequences_list)
        self.__calculte_markov_model_page_rank()



    # Gets the probability to a sequence to occur
    def get_probability_event_sequence(self, sequence: list = None) -> float:
        if self.markov_model == None:
            raise "Markov model is not created"
        else:
            # The probability of the complete sequence will be calculated with the probabilities from all events in sequence
            probability = 1 # If only exist one event is because is the only possible action  
            if len(sequence) > 1:
                seq_len = len(sequence)
                probability = 0                
                for i in range(seq_len -1):
                    seq = list()
                    seq.append(sequence[i])
                    seq.append(sequence[i+1])
                    # Gets the probability for the next event to occur being in the current state
                    probability = probability + self.markov_model.calc_prob_to_page(seq, False)
                probability = probability / len(sequence)    
            return probability

    # Plots the probability matrix heatmap for the Virtual Space Room Markov model
    def plot_markov_probability_matrix_heatmap(self):
        if self.markov_model == None:
            raise "Markov model is not created"
        else:            
            plot.figure(figsize= (20,20))
            sns.heatmap(self.markov_model.prob_matrix,
                        cmap = "coolwarm",
                        linecolor = 'white',
                        linewidths = 0.8,
                        annot = False,
                        fmt = '.2f',
                        xticklabels = self.markov_model.pages,
                        yticklabels = self.markov_model.pages)

            # This sets the yticks "upright" with 0, as opposed to sideways with 90.
            plot.yticks(rotation=0) 

            plot.title('Events sequence probability heatmap matrix. Virtual space: ' + self.virtual_space_name + ", sala: " + self.room, fontsize=24)

            plot.savefig(".\\output\generated_models\\matrix_prob_" + self.virtual_space_name + "_" + self.room + ".png" , dpi=120)
            plot.show()


    # Plots the Chain graph for the Virtual Space Room Markov model
    def plot_markov_probability_chain_graph(self):
        if self.markov_model == None:
            raise "Markov model is not created"
        else:
            if self.max_nodes == 2:
                graph_path_name = ".\\output\generated_models\\"  + self.virtual_space_name + "_sala_"+ self.room + "_gv"
                graph = visualise_markov_chain(self.markov_model)
                graph.render(graph_path_name, view=True)