import datetime as dt
import string
import pandas as pd
from Transformers.OpenSearchVirtualSpaceAnalyticsTransformer import OpenSearchVirtualSpaceAnalyticsTransformer
from Services.ImasconoOpenSearchDataService import ImasconoOpenSearchDataService
from AnomalyDetectors.DiscreteEventsSequenceAnomalyDetector import DiscreteEventsSequenceAnomalyDetector
from AnomalyDetectors.PointAnomalyDetector import PointAnomalyDetector
import os
import warnings



# Class that manages all Anomaly/Bug Detection Process
class AnomalyDetectorCore:

    def __init__(self, space_name:string, start_date: dt.datetime, end_date: dt.datetime, show_point_models: bool, show_sequence_models:bool) -> None:   
        self.space_name = space_name
        self.start_date = start_date
        self.end_date = end_date
        self.show_point_models = show_point_models
        self.show_sequence_models = show_sequence_models

    def __create_process_folders_if_not_exists(self):        
        folder = './output'
        if not os.path.exists(folder):
           os.mkdir(folder) 

        folder = './output/datasets'
        if not os.path.exists(folder):
           os.mkdir(folder) 
        
        folder = './output/generated_models'
        if not os.path.exists(folder):
           os.mkdir(folder) 



    
    def process(self):   
        warnings.filterwarnings("ignore")
        
        
        print('Starting detection process')

        # Creates needed folders for save process results
        self.__create_process_folders_if_not_exists()

        # Creates the service for get Opensearch Virtual space analytics
        data_service = ImasconoOpenSearchDataService()

        # Creates Virtual Space Spaceship Opensearch Analytics transformer
        transformer = OpenSearchVirtualSpaceAnalyticsTransformer(self.space_name)

        # Creates Virtual Space Spaceship Point Anomaly Detector
        point_anomaly_detector = PointAnomalyDetector(self.space_name)

        # Creates Virtual Space Spaceship Sequence Anomaly Detector
        discrete_events_sequence_anomaly_detector = DiscreteEventsSequenceAnomalyDetector(self.space_name)


        if transformer.load_pattern_table() == False:
            print('Process Terminated with Error')  


        # difference between dates in timedelta
        days = (self.end_date - self.start_date).days

        full_dataframe = pd.DataFrame()
        full_sessions_with_one_event = pd.DataFrame()
        # First iteration for creating the Models
        while days > 0:
            date = self.end_date - dt.timedelta(days=days)
            days = days - 1

            print(f"  Getting and preparing OpenSearch {self.space_name} remote analitycs for date: " + date.strftime('%Y-%m-%d'))

            # Get analytics from Virtual Space for a specific date
            opensearch_analytics = data_service.get_data_from_OpenSearch(self.space_name, date)

            # Transform analytics in a DataFrame with Events sequences prepared   
            df_transformed, df_sessions_with_one_event = transformer.transform(opensearch_analytics, date)

            if df_transformed.size > 0:
                full_dataframe = full_dataframe.append(df_transformed)
            
            if df_sessions_with_one_event.size > 0:
                full_sessions_with_one_event = full_sessions_with_one_event.append(df_sessions_with_one_event)



        print('Prepared dataframes with all analytic data')

        # Write Sessions with one event into a csv file
        str_file = f'./output/datasets/{self.space_name}_sessions_with_one_event.csv' 
        full_sessions_with_one_event.to_csv(str_file, index=False) 

        if len(full_sessions_with_one_event)> 0:
            print(f'Sessions with only one event has been found and stored in {str_file}')

        # Write Full dataframe into a csv file
        str_file = f'./output/datasets/{self.space_name}_full_dataframe.csv' 
        full_dataframe.to_csv(str_file, index=False) 

        if len(full_dataframe) == 0:
            print(f'There are no data to process for {self.space_name} from {str(self.start_date)} to {str(self.end_date)}')
            print('Process Terminated')
            return

        # Checks for Point Anomalies
        point_anomaly_detector.create_models(full_dataframe)

        result_df_position, result_df_time = point_anomaly_detector.get_possible_anomalies(full_dataframe)

        str_file = f'./output/datasets/{self.space_name}_position_anomaly_processed.csv' 
        result_df_position.to_csv(str_file, index=False) 
        print(f'     Position anomaly/bug deteccion prediction has been stored in {str_file}')
    
        str_file = f'./output/datasets/{self.space_name}_time_anomaly_processed.csv'
        result_df_time.to_csv(str_file, index=False) 
        print(f'     Time anomaly/bug deteccion prediction has been stored in {str_file}')


        # Checks for Discrete Events Sequences Anomalies
        discrete_events_sequence_anomaly_detector.create_models(full_dataframe)
       
        result_df = discrete_events_sequence_anomaly_detector.get_possible_anomalies(full_dataframe)

        str_file = "./output/datasets/spaceship_chain_anomaly_processed.csv" 
        result_df.to_csv(str_file, index=False)        
        print(f'     Sequence Chain Stream anomalies/bug prediction has been stored in {str_file}')
  
     

        #Shows the plots Point anomaly detector
        if self.show_point_models:
            print('Preparing to show point anomalies/bug detection models')
            point_anomaly_detector.plot_decision_boundaries(full_dataframe)


        #Shows the plots for the Discrete events sequences Markovs models created
        if self.show_sequence_models:
            print('Preparing to show Sequence Chain Stream anomalies/bug detection models')
            discrete_events_sequence_anomaly_detector.show_created_models()

        print('Process Terminated Succesfully')

