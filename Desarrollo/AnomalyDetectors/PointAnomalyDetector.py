import pandas as pd
from Models.PositionAnomalyOutliersModel import PositionAnomalyOutliersModel
from Models.TimeAnomalyOutliersModel import TimeAnomalyOutliersModel

# Class for detecting Point Anomalies
class PointAnomalyDetector:


    def __init__(self, virtual_space_name) -> None:
        
        self.position_room_anomalies_models = {}        
        self.time_room_anomalies_models = {}

        self.virtual_space_name = virtual_space_name
        pass

    # Private method 
    def __create_models(self, room_events: pd.DataFrame, room: str) -> pd.DataFrame:
       
        print(f'  Creating Position anomalies/bug detection model for room: {room}')
        # Position room anomalies model
        if room in self.position_room_anomalies_models:
            self.position_room_anomalies_models[room].construct_model(room_events)            
        else:
            self.position_room_anomalies_models[room] = PositionAnomalyOutliersModel(
                                                            virtual_space_name=self.virtual_space_name, 
                                                            room=room)
            self.position_room_anomalies_models[room].construct_model(room_events)

        
        print(f'  Creating Time anomalies/bug detection model for room: {room}')
        # Time room anomalies model
        if room in self.time_room_anomalies_models:
            self.time_room_anomalies_models[room].construct_model(room_events)            
        else:
            self.time_room_anomalies_models[room] = TimeAnomalyOutliersModel(
                                                            virtual_space_name=self.virtual_space_name,
                                                            room=room)
            self.time_room_anomalies_models[room].construct_model(room_events)



    # Public method
    def create_models(self, df: pd.DataFrame): 
        # Get Virtual Space Rooms - each room will have it's own model
        rooms = df['room_id'].unique()
        room_events = []
        for room in rooms:
            # Create room models
            room_events = df[df['room_id'] == room]
            self.__create_models(room_events, room)


    

    def __process_rom_events(self, room_events: pd.DataFrame, room: str) -> pd.DataFrame:


       print(f'  Predicting Position anomalies/bug detection for room: {room}')

       # Position Anomaly Outliets detection
       self.position_room_anomalies_models[room].fit()
       position_anomalies = self.position_room_anomalies_models[room].predict(room_events)


       print(f'  Predicting Time anomalies/bug detection for room: {room}')

       # Time Anomaly Outliets detection
       self.time_room_anomalies_models[room].fit()
       time_anomalies = self.time_room_anomalies_models[room].predict(room_events)

       return position_anomalies, time_anomalies



    def get_possible_anomalies(self, df: pd.DataFrame) -> pd.DataFrame:
        
        result_df_position = pd.DataFrame()
        result_df_time = pd.DataFrame()
        # Get Virtual Space Rooms - each room will have it's own model
        rooms = df['room_id'].unique()
        room_events = []
        for room in rooms:
            room_events = df[df['room_id'] == room]
            position_df, time_df = self.__process_rom_events(room_events, room)
            if (position_df.empty == False):
                result_df_position = pd.concat([result_df_position, position_df])
            if (time_df.empty == False):
                result_df_time = pd.concat([result_df_time, time_df])


        # Order position dataframe by session and date
        result_df_position = result_df_position.sort_values(by=['user_id','ts'])

        return  result_df_position, result_df_time

        


    def __plot_decision_boundaries(self, room_events: pd.DataFrame, room: str) -> None:
       
       # Position Anomaly Outliets detection       
       self.position_room_anomalies_models[room].plot_decision_boundaries(room_events)

       # Time Anomaly Outliets detection
       self.time_room_anomalies_models[room].plot_decision_boundaries(room_events)
       

    def plot_decision_boundaries(self, df: pd.DataFrame) -> None:
         # Get Virtual Space Rooms - each room will have it's own model
        rooms = df['room_id'].unique()
        room_events = []
        for room in rooms:
            room_events = df[df['room_id'] == room]
            self.__plot_decision_boundaries(room_events, room)
            

        
