import requests
from datetime import datetime

class ImasconoOpenSearchDataService:

    def __init__(self):
        self.username = ""
        self.password = ""
        self.url_analytics = ""

    # Decode Api Request error code
    @staticmethod
    def print_response_error_code(error_code):
        if (error_code == 400): 
            print("Bad request")
        if (error_code == 401):
            print("Authentication error")
        if (error_code == 403):
            print("Forbiden resource access error")
        if (error_code == 404):
            print("The resource wasn't found on the server")
        if (error_code == 503):
            print("Thes server is not ready to handle the request")

    @staticmethod
    def get_index_search_from_date(date):
        format = '%Y.%m.%d'
        index =  "analytics-" + date.strftime(format)
        return index


    # Get Data From OpenSearch API
    def get_data_from_OpenSearch(self, virtual_space_id, date):
    
        index_search = self.get_index_search_from_date(date)            
        
        response = requests.get(self.url_analytics,
                                auth=(self.username, self.password),
                                json= {
                                        "size": "10000",  
                                        "query": {
                                            "bool": {
                                                "must":[ 
                                                    {
                                                        "match": {
                                                            "_index": index_search           
                                                        }
                                                    },
                                                    {
                                                        "match": {
                                                            "virtual_space_id": virtual_space_id        
                                                        }
                                                    }
                                                ]
                                            }
                                        }
                                    }                                      
                                )

        if (response.status_code != 200):
            self.print_response_error_code(response.status_code)
            return {}
        else:
            eventsData = response.json()
            return eventsData
            